<?
$critics = array(
'Lisa Rose' => array(  
				'Lady in the Water' => 2.5,
				'Snakes on a Plane' => 3.5,
				'Just My Luck' => 3.0,
                'Superman Returns' => 3.5,
                'You, Me and Dupree' => 2.5,
                'The Night Listener' => 3.0,
               ),
'Gene Seymour' =>array(
				'Lady in the Water' => 3.0,
				'Snakes on a Plane' => 3.5,
				'Just My Luck' => 1.5, 
                'Superman Returns' => 5.0,
                'The Night Listener' => 3.0,
                'You, Me and Dupree' => 3.5
                 ),
'Michael Phillips' => array(
				'Lady in the Water' => 2.5,
				'Snakes on a Plane' => 3.0,
				'Superman Returns' => 3.5,
                'The Night Listener' => 4.0
                ),
'Claudia Puig' => array(
				'Snakes on a Plane' => 3.5,
				'Just My Luck' => 3.0,
				'The Night Listener' => 4.5, 
                'Superman Returns' => 4.0,
                'You, Me and Dupree' => 2.5
                ),
'Mick LaSalle' => array(
				'Lady in the Water' => 3.0,
				'Snakes on a Plane' => 4.0,
				'Just My Luck' => 2.0, 
                'Superman Returns' => 3.0,
                'The Night Listener' => 3.0,
                'You, Me and Dupree' => 2.0
                ),
'Jack Matthews' => array(
				'Lady in the Water' => 3.0,
				'Snakes on a Plane' => 4.0,
				'The Night Listener' => 3.0, 
                'Superman Returns' => 5.0,
                'You, Me and Dupree' => 3.5
                ),
'Toby' => array('Snakes on a Plane' => 4.5,
				'You, Me and Dupree' => 1.0,
				'Superman Returns' =>4.0)
);
function getrecomendation($username)
{
	global $critics;
	foreach ($critics as $key => $value) {
		if ($key!=$username)
		{
			$c=0;
			foreach ($value as $film => $point)
			{
				$keyfield=$key.'';
				if (abs($critics[$keyfield][$film]-$critics[$username][$film])<=1)
				{
					$c=$c+1;
				}	
			}
		$jakkard[$key]=$c/(count($critics[$username])+count($critics[$key])-$c);	
		}	
	}
	//print_r($jakkard);
	echo "<br/>";
	$qw=0;
	
	foreach ($jakkard as $key => $value) {
		if ($value>0.2) {
			foreach ($critics[$key] as $jakfilmname => $jakfilmpoint) {
				$qq=0;
				foreach ($critics[$username] as $x => $y) {
					if ($x == $jakfilmname){
						$qq=1;					
					}
				}

				if (!$qq && $jakfilmpoint>=3)
				{
					$buffer[$key][$jakfilmname] = $jakfilmpoint;$qw=$qw+1;
				}	
			}
		}
	}
	foreach ($buffer as $human => $films) {
		$proizv=1;
		foreach ($films as $filmname => $point) {
			$resulting[$filmname] = $resulting[$filmname] + $point * $jakkard[$human];
			$resdel[$filmname] = $resdel[$filmname] + 1;
			$jakdel[$filmname] = $jakdel[$filmname] + $jakkard[$human];
		}
	}
	foreach ($resulting as $film => $point) {
		$finish[$film]=$resulting[$film]/$jakdel[$film];
	}
	echo "<table id='table'><tbody><tr><td>Название фильма</td><td>Предполагаемая оценка</td></tr>";
	foreach ($finish as $film => $point){
		echo "<tr><td>",$film,"</td><td>",$point,"</td></tr>";
	}
	echo "</tbody></table>";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Коэффициент Жакарда</title>
	<link rel="stylesheet" type="text/css" href="style.css">	
</head>
<body>
	<div><?getrecomendation('Toby')?></div>
</body>
</html>